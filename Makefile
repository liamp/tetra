Executable=tetra.exe
PDBFile=tetra.pdb
#Sources=code/tetra.cc
GameSources=code/tetra.cc
GeneratorSources=code/resrc_generator.cc
Objects=build/tetra.o

GeneratorBinary=resrc_generator.exe
GeneratedHeader=code\tetra_data.h

Libraries=glad.lib glfw3.lib opengl32.lib kernel32.lib user32.lib gdi32.lib shell32.lib
Compiler=clang
Linker=link -nologo
DisabledWarnings=-Wno-unused-parameter -Wno-c++98-compat -Wno-gnu-anonymous-struct -Wno-old-style-cast
CompilerFlags=-isystem include -Wall -Wextra -Weverything -Wpadded -std=c++11 -mavx -fno-omit-frame-pointer -fno-exceptions -fno-rtti -g -gcodeview $(DisabledWarnings)
LinkerFlags=-incremental:no -debug -entry:mainCRTStartup -libpath:libs -machine:x64 -nodefaultlib:libcmt $(Libraries)

all: $(Executable)

$(Objects): $(GameSources) $(GeneratedHeader)
	@$(Compiler) -c $(CompilerFlags) $(GameSources) -o $@ 

$(Executable): $(Objects) tags
	@$(Linker) $(Objects) $(LinkerFlags) -out:$(Executable)

$(GeneratedHeader): $(GeneratorBinary)
	@$(GeneratorBinary) > $@

$(GeneratorBinary): $(GeneratorSources)
	@$(Compiler) $(CompilerFlags) -std=c++14 -Wno-c++98-c++11-compat-binary-literal $? -o $@

clean:
	@del build\*.o 
	@del tags
	@del $(GeneratedHeader)
	@del $(Executable)
	@del *.pdb

tags:
	@ctags -R code

run: $(Executable)
	@$(Executable)

