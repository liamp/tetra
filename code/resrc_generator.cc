#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#include "tetra.h"
#include "tetra_world.h"


struct cell
{
    u64 PackedShape;
    u8  Index;
};


// NOTE(liam): Non-indexed meshes
static u8 TetrahedronMesh[12] = {
    0, 1, 2,  2, 0, 4,  4, 1, 2,  0, 1, 4
};

static u8 HalfCubicMesh[24] = {
    1, 0, 2,  2, 3, 1,  1, 3, 5,  5, 3, 2, 
    2, 5, 4,  4, 0, 5,  5, 0, 1,  0, 4, 2
};

static u8 CubicMesh[36] = {
    1, 0, 2,  2, 3, 1,  1, 3, 5,  5, 3, 7,
    7, 3, 6,  6, 3, 2,  2, 6, 0,  0, 4, 6,
    6, 4, 7,  7, 4, 5,  5, 4, 0,  0, 1, 5
};

static u8 SideHalfCubicMesh[24] = {
    1, 0, 2,  2, 3, 1,  1, 3, 4,  4, 3, 6,
    6, 3, 2,  2, 6, 0,  0, 4, 6,  0, 1, 4
};

static u8 QuarterPyramidMesh[18] = {
    1, 0, 2,  2, 5, 1,  1, 5, 0,  0, 5, 4, 
    4, 5, 2,  2, 0, 4
};

static u8 AntitetrahedralMesh[30] = {
    0, 2, 1,  1, 2, 3,  3, 2, 6,  6, 2, 0,
    0, 4, 6,  6, 4, 5,  5, 4, 0,  0, 1, 5,
    5, 1, 3,  3, 6, 5

};

static u8 AntiQuarterPyramidMesh[27] = {
    1, 0, 2,  2, 3, 1,  1, 5, 0,  0, 5, 4,
    4, 5, 6,  6, 2, 0,  0, 4, 6,  6, 5, 1, 
    1, 3, 6
};


static const cell TerrainShapeCells[] = {
//      7  6  5  4  3  2  1  0      7654 3210
    /*{ 0x00'00'00'00'00'00'00'08,  0b0000'0001 },  // Single tetrahedron in a corner
    { 0x00'00'00'00'00'00'18'18,  0b0000'0011 },  // Two half-cubes side-by-side
    { 0x00'00'00'00'00'38'00'38,  0b0000'0101 },  // Sideways half-cubes stacked on top of each other
    { 0x00'00'08'28'00'00'28'30,  0b0001'0011 },  // Base sliced diagonally
    { 0x00'00'30'30'00'00'30'30,  0b0011'0011 },  // Flat base
    { 0x00'08'08'28'08'28'28'30,  0b0001'0111 },  // Triangular 
    { 0x18'18'30'30'30'30'30'30,  0b0011'1111 },  // Half slice
    { 0x00'00'08'28'29'00'30'30,  0b0001'1011 },  // L-shape with antitetrahedrons
    { 0x0f'00'00'00'00'00'00'08,  0b1000'0001 },  // Diagonally opposite tetrahedra
    { 0x30'30'30'30'30'30'30'30,  0b1111'1111 },  // Full cube
    { 0x00'00'00'00'08'28'28'30,  0b0000'0111 },  // L-shape
    { 0x28'30'30'30'30'30'30'30,  0b0111'1111 },  // Cube with shaved off corner
    { 0x3d'00'3d'00'00'38'00'38,  0b1010'0101 },  // Diagonally opposite stacks
    { 0x00'08'30'30'08'28'30'30,  0b0011'0111 },
    { 0x00'08'08'28'30'30'30'30,  0b0001'1111 },
    { 0x38'30'38'30'30'30'38'30,  0b0101'1111 },
    { 0x08'08'08'08'08'08'08'08,  0b1000'1101 },*/

    /*{ 0x30'30'30'30'30'30'30'30,  0b1111'1111 }, // Full cube
    { 0x00'00'00'00'00'00'00'08,  0b0000'0001 }, // Single corner 
    { 0x00'00'00'00'00'00'18'18,  0b0000'0011 }, // Two half-cubes side by side
    { 0x00'00'00'00'0b'00'00'08,  0b0000'1001 }, // Diagonally opposite corners on the same face
    { 0x00'00'00'0c'00'00'09'00,  0b0001'0010 }, // Diagonally opposite corners on the same face (flipped vertically)
    { 0x00'00'00'00'00'0a'09'00,  0b0000'0110 }, // Diagonally opposite corners on the same face (flipped)
    { 0x00'00'08'28'00'00'28'30,  0b0001'0011 }, // Base sliced diagonally
    { 0x00'00'30'30'00'00'30'30,  0b0011'0011 }, // Cube sliced in half lengthways
    { 0x0f'00'08'28'00'00'28'30,  0b1001'0011 }, // Base sliced diagonally with opposite tetrahedron
    { 0x0f'00'00'0c'00'0a'09'00,  0b1001'0110 }, // Alternating corners
    { 0x00'08'08'28'08'28'28'30,  0b0001'0111 }, // Triangular sliced face
    { 0x00'00'29'09'08'28'30'30,  0b0010'0111 }, // S-shape around side
    { 0x00'00'00'00'00'38'00'38,  0b0000'0101 }, // Vertical stack
    { 0x00'00'00'00'08'28'28'30,  0b0000'0111 }, // L-shape
    { 0x00'00'00'00'29'09'30'29,  0b0000'1011 }, // L-shape (flipped horizontally)
    { 0x00'00'00'00'30'2b'2b'0b,  0b0000'1110 }, // L-shape (flipped vertically)
    { 0x30'30'30'30'30'30'30'2f,  0b1111'1110 }, // Cube with shaved off corner
    { 0x30'30'30'30'30'30'30'30,  0b1111'1111 }, // Full cube
    { 0x18'18'30'30'30'30'30'30,  0b0011'1111 }, // Cube with top-front corners sliced off
    { 0x38'30'38'30'30'30'38'30,  0b0101'1111 }, // Cube sliced in half vertically
    { 0x00'08'30'30'08'28'30'30,  0b0011'0111 }, // Sloping frustum
    { 0x00'08'08'28'30'30'30'30,  0b0001'1111 }, // Sloping frustum (flipped)
    { 0x00'08'08'28'30'30'30'30,  0b0001'1111 }, // Sloping frustum (flipped)
    { 0x00'08'08'28'30'30'30'30,  0b0001'1111 }, // Sloping frustum (upside down)*/

    { 0x30'30'30'30'30'30'30'30,  0b1111'1111 }, // Full cube
    //{ 0x00'00'00'00'00'00'00'00,  0b0000'0000 }, // Nothing


    { 0x00'00'00'00'00'00'00'08,  0b0000'0001 }, // Single tetrahedron in a corner
    { 0x30'30'30'30'30'30'30'2f,  0b1111'1110 }, // Full cube with shaved-off corner

    { 0x00'00'00'00'00'38'00'38,  0b0000'0101 }, // Vertical stack
    { 0x30'30'30'30'30'3d'30'3d,  0b1111'1010 }, // Cube with one vertical corner cut off

    { 0x3f'00'3f'00'00'38'00'38,  0b1010'0101 }, // Diagonally opposite vertical stacks
    { 0x30'30'38'30'30'3d'30'3d,  0b0101'1010 }, // Cube with both vertical corners cut off

    { 0x00'00'00'00'00'00'18'18,  0b0000'0011 }, // Two half-cubes side by side
    { 0x18'18'30'30'30'30'30'30,  0b0011'1111 }, // Full cube with top-front corners shaved off

    { 0x1f'1e'00'00'00'00'18'18,  0b1100'0011 }, // Diagonally opposite double half-cubes
    { 0x18'18'30'30'30'30'1f'1e,  0b0011'1100 }, // Cube with top-front and bottom-back corners cut off

    { 0x00'08'08'28'08'28'28'30,  0b0001'0111 }, // Triangular sliced face
    { 0x30'2f'2f'0f'2f'0f'0f'00,  0b1110'1000 }, // Triangular sliced face (complement)

    { 0x00'00'30'30'00'00'30'30,  0b0011'0011 }, // Bottom flat base
    { 0x30'30'00'00'30'30'00'00,  0b1100'1100 }, // Top flat base

    { 0x00'00'29'09'08'28'30'30,  0b0010'0111 }, // S-shape around right side

    { 0x00'00'08'28'00'00'28'30,  0b0001'0011 }, // L-shape (1)
    { 0x30'30'2f'0f'30'30'0f'00,  0b1110'1100 }, // Frustum (1)

    { 0x00'00'00'00'08'28'28'30,  0b0000'0111 }, // L-shape (2)
    { 0x30'30'30'30'2f'0f'0f'00,  0b1111'1000 }, // Frustum (2)

    { 0x00'08'00'28'00'28'00'30,  0b0001'0101 }, // L-shape (3)
    { 0x30'2f'30'0f'30'0f'30'00,  0b1110'1010 }, // Frustum (3)

    /*{ 0x00'00'00'00'08'28'28'30,  0b0000'0111 }, // L-shape (side)
    { 0x30'30'30'30'2f'0f'0f'00,  0b1111'1000 }, // Frustum (side)

    { 0x00'00'08'28'00'00'28'30,  0b0001'0011 }, // L-shape (bottom)
    { 0x30'30'2f'0f'30'30'0f'00,  0b1110'1100 }, // Frustum (bottom)

    { 0x00'00'00'00'30'2b'2b'0b,  0b0000'1110 }, // L-shape (front)
    { 0x30'30'30'30'00'0c'0c'2c,  0b1111'0001 }, // Frustum (front)*/

    //{ 0x38'30'38'30'30'3d'30'3d,  0b0101'1010 }, // Cube with both vertical corners cut off
};


static u8 RotatedVertices[8][8] = {
   // A  B  C  D  E  F  G  H
    { 0, 1, 2, 3, 4, 5, 6, 7 }, // 0 -- 0 0 0
    { 1, 5, 3, 7, 0, 4, 2, 6 }, // 1 -- 0 0 -90
    { 2, 3, 6, 7, 0, 1, 4, 5 }, // 2 -- 0 90 0
    { 3, 7, 2, 6, 1, 5, 0, 4 }, // 3 -- 90 90 0
    { 4, 0, 6, 2, 5, 1, 7, 3 }, // 4 -- 0  0  90 
    { 5, 4, 7, 6, 1, 0, 3, 2 }, // 5 -- 0  0  180
    { 6, 2, 7, 3, 4, 0, 5, 1 }, // 6 -- 0  90  90
    { 7, 6, 3, 2, 5, 4, 1, 0 }, // 7 -- 0  90 180
};

static u8 VoxelMeshSizes[8] = {
    0,
    ArrayCount(TetrahedronMesh),
    ArrayCount(HalfCubicMesh),
    ArrayCount(QuarterPyramidMesh),
    ArrayCount(AntiQuarterPyramidMesh),
    ArrayCount(AntitetrahedralMesh),
    ArrayCount(CubicMesh),
    ArrayCount(SideHalfCubicMesh),
};

static inline u64
RotateVoxel(u64 Shape, u64 Orientation)
{
    return (Shape | Orientation);
}

static inline u64
GetVoxelFromCell(u64 Cell, int VoxelIndex)
{
    return (Cell >> (8 * (VoxelIndex))) & 0xff;
}


static cell
RotateCell(cell Cell, int Rotation)
{
    cell Result;
    u64 NewPackedShape = 0;
    u8  NewIndex = 0;

    for (int VoxelIndex = 0; VoxelIndex < 8; ++VoxelIndex)
    {
        int BitIndex = (1 << VoxelIndex);
        
        // NOTE: Check if this voxel is set in index; if it is rotate the
        // index bit.
        if (Cell.Index & BitIndex)
        {
            int NewBitIndex = RotatedVertices[Rotation][VoxelIndex];
            NewIndex |= (1 << NewBitIndex);
        }

        u64 CellVoxel = GetVoxelFromCell(Cell.PackedShape, VoxelIndex);
        if (CellVoxel != 0)
        {
            u64 BaseShape = CellVoxel & 0x38;    // NOTE: High 3 bits
            u64 BaseRotation = CellVoxel & 0x07; // NOTE: Low 3 bits

            u64 NewRotation = RotatedVertices[Rotation][BaseRotation];
            u64 RotatedVoxel = RotateVoxel(BaseShape, NewRotation);
            int NewVoxelIndex = RotatedVertices[Rotation][VoxelIndex];
            NewPackedShape |= (RotatedVoxel << (8 * NewVoxelIndex));
        }

    }

    Result.PackedShape = NewPackedShape;
    Result.Index = NewIndex;

    return Result;
}

static void
OutputTerrainShapeLookup()
{
    static u64 TerrainShapeLookup[256];

    for (int BaseCaseIndex = 0; BaseCaseIndex < ArrayCount(TerrainShapeCells); ++BaseCaseIndex)
    {
        cell BaseCaseCell = TerrainShapeCells[BaseCaseIndex];

        for (int Rotation = 0; Rotation < 8; ++Rotation)
        {
            cell RotatedCell = RotateCell(BaseCaseCell, Rotation);

            TerrainShapeLookup[RotatedCell.Index] = RotatedCell.PackedShape;
        }
    }

    printf("static const u64 TerrainCellShapes[256] = {\n");
    for (int CellIndex = 0; CellIndex < 256; ++CellIndex)
    {
        printf("   0x%016llx, // 0x%02x\n", TerrainShapeLookup[CellIndex], CellIndex);
        //if ((CellIndex + 1) % 2 == 0) printf("\n");

    }
    printf("\n};\n");
}


static void
OutputShapeRotations(const u8* const ShapeVertices, int VertexCount)
{
    for (int Rotation = 0; Rotation < 8; ++Rotation)
    {
        printf("    { ");
        for (int Vertex = 0; Vertex < 36; ++Vertex)
        {

            if (Vertex < VertexCount)
            {
                int V = ShapeVertices[Vertex];
                printf("%d,", RotatedVertices[Rotation][V]);
            }
            else
            {
                printf("Z,");
            }

        }
        printf("}, ");

        printf("\n");
    }
}

static void
OutputIndexMeshSizes()
{
    printf("static const u8 VoxelMeshIndexDataSizes[8] = {\n");
    for (int Index = 0; Index < ArrayCount(VoxelMeshSizes); ++Index)
    {
        printf("    %hhu,\n", VoxelMeshSizes[Index]);
    }
    printf("};\n");
}

static void
OutputShapeTable()
{
    printf("static const u8 VoxelMeshes[64][36] = {\n");
    printf("    #define Z 0xff\n");
    OutputShapeRotations(nullptr, 0);
    OutputShapeRotations(TetrahedronMesh, 12);
    OutputShapeRotations(QuarterPyramidMesh, 18);
    OutputShapeRotations(HalfCubicMesh, 24);
    OutputShapeRotations(AntiQuarterPyramidMesh, 27);
    OutputShapeRotations(AntitetrahedralMesh, 30);
    OutputShapeRotations(CubicMesh, 36);
    OutputShapeRotations(SideHalfCubicMesh, 24);
    printf("    #undef Z\n");
    printf("};\n");
}

static void
OutputHeaderPrologue()
{
    printf("#ifndef TETRA_DATA_H\n#define TETRA_DATA_H\n\n");
    printf("// WARNING: This file is automatically generated - do not edit!\n\n");
    printf("namespace Data {\n");
}

static void
OutputHeaderEpilogue()
{
    printf("} // namespace Data\n");
    printf("\n#endif /*TETRA_GENERATED*/\n");
}


extern int
main(int ArgCount, const char** const Args)
{
    OutputHeaderPrologue();
    {
        OutputShapeTable(); 
        OutputIndexMeshSizes();
        OutputTerrainShapeLookup();
    }
    OutputHeaderEpilogue();

    return EXIT_SUCCESS;
}
