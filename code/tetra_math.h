#ifndef TETRA_MATH_H
#define TETRA_MATH_H

#include <math.h>

#define Pi32  3.14159265f
#define Tau32 6.28318531f

#define Sqrt2     1.41421569f
#define InvSqrt2  0.70710678f
#define Cot45Deg  1.0f
#define PiOver180 0.01745329f
#define Epsilon32 1E-5

#define Fade(X) ((X*X*X)*((6.0f*X*X) - (15.0f*X) + 10.0f))
#define Lerp(Start, End, Fract) ((Start + Fract * (End - Start)))

#define Rads(Deg) ((Deg)*PiOver180)



// {{{ Vectors
// NOTE: COLUMN vector.
struct v3
{
    float X;
    float Y;
    float Z;

    inline v3() {}

    inline explicit
    v3(float InX, float InY, float InZ)
    {
        X = InX;
        Y = InY;
        Z = InZ;
    }
};

inline v3 vcall
operator+(v3 L, v3 R)
{
    v3 Result;

    Result.X = L.X + R.X;
    Result.Y = L.Y + R.Y;
    Result.Z = L.Z + R.Z;

    return Result;
}

inline v3 vcall
operator-(v3 L, v3 R)
{
    v3 Result;

    Result.X = L.X - R.X;
    Result.Y = L.Y - R.Y;
    Result.Z = L.Z - R.Z;

    return Result;
}

inline v3 vcall
operator/(v3 V, f32 Scalar)
{
    v3 Result;

    Result.X = V.X / Scalar;
    Result.Y = V.Y / Scalar;
    Result.Z = V.Z / Scalar;

    return Result;
}

inline v3 vcall
operator-(v3 A)
{
    return v3(-A.X, -A.Y, -A.Z);
}


inline v3& vcall
operator+=(v3& L, v3 R)
{
    L = L + R;

    return L;
}


inline v3& vcall
operator-=(v3& L, v3 R)
{
    L = L - R;

    return L;
}


inline v3 vcall
operator*(float Scalar, v3 Vec)
{
    v3 Result;

    Result.X = Scalar * Vec.X;
    Result.Y = Scalar * Vec.Y;
    Result.Z = Scalar * Vec.Z;

    return Result;
}


static inline v3 vcall
Cross(v3 U, v3 V)
{
    v3 Result;

    Result.X = (U.Y * V.Z) - (U.Z * V.Y);
    Result.Y = (U.Z * V.X) - (U.X * V.Z);
    Result.Z = (U.X * V.Y) - (U.Y * V.X);

    return Result;
}

static inline float
Dot(v3 U, v3 V)
{
    float Result = (U.X * V.X) + (U.Y * V.Y) + (U.Z * V.Z);
    return Result;
}

static inline v3 vcall
HadamardProduct(v3& A, v3& B)
{
    v3 Result;

    Result.X = A.X * B.X;
    Result.Y = A.Y * B.Y;
    Result.Z = A.Z * B.Z;

    return Result;
}

static inline v3 vcall
Normalize(v3 V)
{
    const float L = sqrtf(V.X*V.X + V.Y*V.Y + V.Z*V.Z);

    if (L == 0.0f)
    { 
        return v3(0.0f, 0.0f, 0.0f);
    }
    else
    {
        v3 Result;

        Result.X = V.X / L;
        Result.Y = V.Y / L;
        Result.Z = V.Z / L;

        return Result;
    }
}

static inline float vcall
Length(v3 V)
{
    return sqrtf(V.X*V.X + V.Y*V.Y + V.Z*V.Z);
}

// }}}


// {{{ Integer vectors
struct uv3
{
    u32 X;
    u32 Y;
    u32 Z;

    inline uv3() {}

    inline explicit
    uv3(u32 InX, u32 InY, u32 InZ)
    {
        X = InX;
        Y = InY;
        Z = InZ;
    }
};

inline uv3 vcall
HorzMultiply(uv3 L, uv3 R)
{
    return uv3(L.X*R.X, L.Y*R.Y, L.Z*R.Z);
}

inline u32 vcall
HorzSum(uv3 V)
{
    return (V.X + V.Y + V.Z);
}

inline uv3 vcall
operator+(uv3 Left, uv3 Right)
{
    uv3 Result;

    Result.X = Left.X + Right.X;
    Result.Y = Left.Y + Right.Y;
    Result.Z = Left.Z + Right.Z;

    return Result;
}

inline uv3 vcall
operator*(uv3 L, u32 R)
{
    uv3 Result;

    Result.X = L.X * R;
    Result.Y = L.Y * R;
    Result.Z = L.Z * R;

    return Result;
}


// }}}


// {{{ Matrices
// NOTE: ROW-MAJOR matrix. This means that when passing these
// matrices to OpenGL via uniforms you will need to set the 
// 'transpose' parameter to GL_TRUE.
struct m4x4
{
    float M[4][4];
};



static inline m4x4
IdentityMatrix()
{
    m4x4 I = {{
        { 1.0f, 0.0f, 0.0f, 0.0f },
        { 0.0f, 1.0f, 0.0f, 0.0f },
        { 0.0f, 0.0f, 1.0f, 0.0f },
        { 0.0f, 0.0f, 0.0f, 1.0f }
    }};

    return I;
}

static inline m4x4 vcall
TranslationMatrix(const v3 Translation)
{
    m4x4 T = IdentityMatrix();
    
    T.M[0][3] = Translation.X;
    T.M[1][3] = Translation.Y;
    T.M[2][3] = Translation.Z;

    return T;
}

static inline m4x4 vcall
TranslationMatrix(const uv3 Translation)
{
    m4x4 T = IdentityMatrix();
    
    T.M[0][3] = Translation.X;
    T.M[1][3] = Translation.Y;
    T.M[2][3] = Translation.Z;

    return T;
}

static inline m4x4 vcall
TranslationMatrix(u32 X, u32 Y, u32 Z)
{

    m4x4 T = IdentityMatrix();
    
    T.M[0][3] = X;
    T.M[1][3] = Y;
    T.M[2][3] = Z;

    return T;
}

static inline void vcall
Translate3D(m4x4* Matrix, const v3 Translation)
{
    Matrix->M[0][3] = Translation.X;
    Matrix->M[1][3] = Translation.Y;
    Matrix->M[2][3] = Translation.Z;
}


// NOTE: The projection is specified to be in the NEGATIVE z direction.
// 'CotHalfFov' means '1.0 / tan(Fov/2)'
static inline m4x4
PerspectiveProjection(float CotHalfFov, float AspectRatio, float Zn, float Zf)
{
    const float ScaleX = CotHalfFov;
    const float ScaleY = CotHalfFov * AspectRatio;
    const float ScaleZ = -(Zf + Zn) / (Zf - Zn);
    const float OffsetZ = -(2.0f * Zn * Zf) / (Zf - Zn);
    const float ScaleW = -1.0f;

    m4x4 P = {{
        { ScaleX, 0.0f,   0.0f,   0.0f },
        { 0.0f,   ScaleY, 0.0f,   0.0f },
        { 0.0f,   0.0f,   ScaleZ, OffsetZ },
        { 0.0f,   0.0f,   ScaleW,   0.0f } 
    }};

    return P;
}

static inline m4x4
operator*(m4x4 L, m4x4 R)
{
    m4x4 O;
    O.M[0][0] = (L.M[0][0] * R.M[0][0]) + (L.M[0][1] * R.M[1][0]) + (L.M[0][2] * R.M[2][0]) + (L.M[0][3] * R.M[3][0]);
    O.M[0][1] = (L.M[0][0] * R.M[0][1]) + (L.M[0][1] * R.M[1][1]) + (L.M[0][2] * R.M[2][1]) + (L.M[0][3] * R.M[3][1]);
    O.M[0][2] = (L.M[0][0] * R.M[0][2]) + (L.M[0][1] * R.M[1][2]) + (L.M[0][2] * R.M[2][2]) + (L.M[0][3] * R.M[3][2]);
    O.M[0][3] = (L.M[0][0] * R.M[0][3]) + (L.M[0][1] * R.M[1][3]) + (L.M[0][2] * R.M[2][3]) + (L.M[0][3] * R.M[3][3]);

    O.M[1][0] = (L.M[1][0] * R.M[0][0]) + (L.M[1][1] * R.M[1][0]) + (L.M[1][2] * R.M[2][0]) + (L.M[1][3] * R.M[3][0]);
    O.M[1][1] = (L.M[1][0] * R.M[0][1]) + (L.M[1][1] * R.M[1][1]) + (L.M[1][2] * R.M[2][1]) + (L.M[1][3] * R.M[3][1]);
    O.M[1][2] = (L.M[1][0] * R.M[0][2]) + (L.M[1][1] * R.M[1][2]) + (L.M[1][2] * R.M[2][2]) + (L.M[1][3] * R.M[3][2]);
    O.M[1][3] = (L.M[1][0] * R.M[0][3]) + (L.M[1][1] * R.M[1][3]) + (L.M[1][2] * R.M[2][3]) + (L.M[1][3] * R.M[3][3]);

    O.M[2][0] = (L.M[2][0] * R.M[0][0]) + (L.M[2][1] * R.M[1][0]) + (L.M[2][2] * R.M[2][0]) + (L.M[2][3] * R.M[3][0]);
    O.M[2][1] = (L.M[2][0] * R.M[0][1]) + (L.M[2][1] * R.M[1][1]) + (L.M[2][2] * R.M[2][1]) + (L.M[2][3] * R.M[3][1]);
    O.M[2][2] = (L.M[2][0] * R.M[0][2]) + (L.M[2][1] * R.M[1][2]) + (L.M[2][2] * R.M[2][2]) + (L.M[2][3] * R.M[3][2]);
    O.M[2][3] = (L.M[2][0] * R.M[0][3]) + (L.M[2][1] * R.M[1][3]) + (L.M[2][2] * R.M[2][3]) + (L.M[2][3] * R.M[3][3]);

    O.M[3][0] = (L.M[3][0] * R.M[0][0]) + (L.M[3][1] * R.M[1][0]) + (L.M[3][2] * R.M[2][0]) + (L.M[3][3] * R.M[3][0]);
    O.M[3][1] = (L.M[3][0] * R.M[0][1]) + (L.M[3][1] * R.M[1][1]) + (L.M[3][2] * R.M[2][1]) + (L.M[3][3] * R.M[3][1]);
    O.M[3][2] = (L.M[3][0] * R.M[0][2]) + (L.M[3][1] * R.M[1][2]) + (L.M[3][2] * R.M[2][2]) + (L.M[3][3] * R.M[3][2]);
    O.M[3][3] = (L.M[3][0] * R.M[0][3]) + (L.M[3][1] * R.M[1][3]) + (L.M[3][2] * R.M[2][3]) + (L.M[3][3] * R.M[3][3]);

    return O;
}
// }}}


// {{{ Quaternion
// NOTE: Quaternion real component is stored in the X-value
struct quat
{
    float X, Y, Z, W;
};

inline quat 
operator*(quat L, quat R)
{
    // NOTE: Quaternion Multiplication Derivation
    // RULES: 
    //   ij = k   ji = -k
    //   jk = i   kj = -i
    //   ki = j   ik = -j
    //
    //         left                right
    //   (a + bi + cj + dk) * (w + xi + yj + zk)
    //    LX  LY   LZ   LW     RX  RY   RZ   RW
    //
    //   = [a.w + a.xi + a.yj + a.zk]
    //   + [bi.w + bi.xi + bi.yj + bi.zk]
    //   + [cj.w + cj.xi + cj.yj + cj.zk]
    //   + [dk.w + dk.xi + dk.yj + dk.zk]
    //
    //   = [a.w + a.xi + a.yj + a.zk]
    //   + [bi.w - b.x + by.k - bz.j]
    //   + [cj.w - cx.k - c.y + cz.i]
    //   + [dk.w + dx.j - dy.i - d.z]
    //
    //   = [a.w  - b.x  - c.y  - d.z]
    //   + [ax.i + bw.i + cz.i - dy.i]
    //   + [ay.j - bz.j + cj.w + dx.j]
    //   + [az.k + by.k - cx.k + dw.k]
    //
    //   = (aw - bx - cy - dz,
    //      ax + bw + cz - dy,
    //      ay - bz + cw + dx,
    //      az + by - cx + dw)
    quat Result;

    //         left                right
    //   (a + bi + cj + dk) * (w + xi + yj + zk)
    //    LX  LY   LZ   LW     RX  RY   RZ   RW
    Result.X = (L.X * R.X) - (L.Y * R.Y) - (L.Z * R.Z) - (L.W * R.W);
    Result.Y = (L.X * R.Y) + (L.Y * R.X) + (L.Z * R.W) - (L.W * R.Z); 
    Result.Z = (L.X * R.Z) - (L.Y * R.W) + (L.Z * R.X) + (L.W * R.Y);
    Result.W = (L.X * R.W) + (L.Y * R.Z) - (L.Z * R.Y) + (L.W * R.X);

    return Result;
}

static inline quat
RotationQuaternion(const float Angle, v3 Axis)
{
    quat Result;

    Result.X = cosf(Angle / 2.0f);

    Result.Y = Axis.X * sinf(Angle / 2.0f);
    Result.Z = Axis.Y * sinf(Angle / 2.0f);
    Result.W = Axis.Z * sinf(Angle / 2.0f);

    return Result;
}

static inline quat
Conjugate(quat Q)
{
    quat Conj;

    Conj.X = Q.X;

    Conj.Y = -Q.Y;
    Conj.Z = -Q.Z;
    Conj.W = -Q.W;

    return Conj;
}

static inline float
QuatLength(quat Q)
{
    return sqrtf(Q.X*Q.X + Q.Y*Q.Y + Q.Z*Q.Z + Q.W*Q.W);
}

static inline quat
Normalize(quat Q)
{
    quat Result;
    float Length = QuatLength(Q);

    Result.X = Q.X / Length;
    Result.Y = Q.Y / Length;
    Result.Z = Q.Z / Length;
    Result.W = Q.W / Length;

    return Result;
}

static inline v3
ImaginaryPart(quat Q)
{
    return v3(Q.Y, Q.Z, Q.W);
}

static inline float
RealPart(quat Q)
{
    return Q.X;
}

// TODO Needs optimization!
// There is a better way invented by Fabien 'ryg' Giesen.
static inline v3
Rotate(quat Rotation, v3 V)
{
    /*v3 Result;
    Normalize(Rotation);
    const quat Conj = Conjugate(Rotation);
    const quat V = {{ 0.0f, P.X, P.Y, P.Z }};

    quat Rotated = Rotation * V * Conj;

    Result.X = Rotated.Y;
    Result.Y = Rotated.Z;
    Result.Z = Rotated.W;

    return Result;*/
    v3 T = 2.0f * Cross(ImaginaryPart(Rotation), V);
    v3 Rotated = V + RealPart(Rotation) * T + Cross(ImaginaryPart(Rotation), T);

    return Rotated;

}

// }}}


// {{{ Perlin Noise
static inline float
GradDotDst(int Hash, float X, float Y, float Z)
{
    switch (Hash & 0xF)
    {
        case 0x0: return  X + Y;
        case 0x1: return -X + Y;
        case 0x2: return  X - Y;
        case 0x3: return -X - Y;
        case 0x4: return  X + Z;
        case 0x5: return  X - Z;
        case 0x6: return -X + Z;
        case 0x7: return -X - Z;
        case 0x8: return  Y + Z;
        case 0x9: return -Y + Z;
        case 0xA: return  Y - Z;
        case 0xB: return -Y - Z;
        case 0xC: return  Y + X;
        case 0xD: return -Y + X;
        case 0xE: return  Y - X;
        case 0xF: return -Y - Z;
        default: return 0; // Shouldn't happen
    }
}


static inline float
Perlin3(float X, float Y, float Z)
{
    // Randomly distributed values from 0-255
    // P - Permutation Table
    static unsigned char Pt[256] = {
        151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,
        225,140,36,103,30,69,142,8,99,37,240,21,10,23,190, 
        6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,
        35,11,32,57,177,33, 88,237,149,56,87,174,20,125,136,
        171,168, 68,175,74,165,71,134,139,48,27,166, 77,146,
        158,231,83,111,229,122,60,211,133,230,220,105,92,41,
        55,46,245,40,244, 102,143,54, 65,25,63,161, 1,216,80,
        73,209,76,132,187,208, 89,18,169,200,196,135,130,116,
        188,159,86,164,100,109,198,173,186, 3,64,52,217,226,
        250,124,123,5,202,38,147,118,126,255,82,85,212,207,
        206,59,227,47,16,58,17,182,189,28,42,223,183,170,
        213,119,248,152, 2,44,154,163, 70,221,153,101,155,
        167, 43,172,9,129,22,39,253, 19,98,108,110,79,113,224,
        232,178,185, 112,104,218,246,97,228, 251,34,242,193,
        238,210,144,12,191,179,162,241, 81,51,145,235,249,14,
        239,107,49,192,214, 31,181,199,106,157,184, 84,204,
        176,115,121,50,45,127, 4,150,254,138,236,205,93,222,
        114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
      };

    // Unit cube Left-Bottom-Back coordinates
    const int Xi = ((int)X & 255);
    const int Yi = ((int)Y & 255);
    const int Zi = ((int)Z & 255);

    // Coords of our point inside its unit cube.
    const float Xf = X - (int)X;
    const float Yf = Y - (int)Y;
    const float Zf = Z - (int)Z;

    // Smooth point coords towards integral values.
    // This produces smoother noise output.
    const float U = Fade(Xf);
    const float V = Fade(Yf);
    const float W = Fade(Zf);

    // Hash values for the corners of the unit cube.
    // Named according to their positions - assume we start
    // at (0, 0, 0) and the cube sides are unit length.
    int C000, C010, C001, C011, C100, C110, C101, C111;

    // Hash values are calculted using the permutation
    // table.
    C000 = Pt[Pt[Pt[Xi] + Yi] + Zi];
    C010 = Pt[Pt[Pt[Xi] + Yi + 1] + Zi];
    C001 = Pt[Pt[Pt[Xi] + Yi] + Zi + 1];
    C011 = Pt[Pt[Pt[Xi] + Yi + 1] + Zi + 1];
    C100 = Pt[Pt[Pt[Xi + 1] + Yi] + Zi];
    C110 = Pt[Pt[Pt[Xi + 1] + Yi + 1] + Zi];
    C101 = Pt[Pt[Pt[Xi + 1] + Yi] + Zi + 1];
    C111 = Pt[Pt[Pt[Xi + 1] + Yi + 1] + Zi + 1];

    float X1, X2, Y1, Y2;
    X1 = Lerp(GradDotDst(C000, Xf, Yf, Zf), GradDotDst(C100, Xf - 1.0f, Yf, Zf), U);
    X2 = Lerp(GradDotDst(C010, Xf, Yf - 1.0f, Zf), GradDotDst(C110, Xf - 1.0f, Yf - 1.0f, Zf), U);
    Y1 = Lerp(X1, X2, V);

    X1 = Lerp(GradDotDst(C001, Xf, Yf, Zf - 1.0f), GradDotDst(C101, Xf - 1.0f, Yf, Zf - 1.0f), U);
    X2 = Lerp(GradDotDst(C011, Xf, Yf - 1.0f, Zf - 1.0f), GradDotDst(C111, Xf -1.0f, Yf - 1.0f, Zf - 1.0f), U);
    Y2 = Lerp(X1, X2, V);

    return (Lerp(Y1, Y2, W) + 1) / 2.0f;
}

static inline float
Perlin3(float X, float Y, float Z, const int Octaves, const float Persistance)
{
    float NoiseVal = 0.0f;
    float MaxVal = 0.0f;

    float Frequency = 1.0f;
    float Amplitude = 1.0f;

    for (int Octave = 0; Octave < Octaves; ++Octave)
    {
        NoiseVal += Perlin3(X * Frequency, Y * Frequency, Z * Frequency) * Amplitude;
        MaxVal += Amplitude;

        Amplitude += Persistance;
        Frequency *= 2;
    }

    return NoiseVal / MaxVal;
}
// }}}


#endif

