#ifndef TETRA_H
#define TETRA_H

#include <stdint.h>

#if defined(__LLVM__) || defined(_MSC_VER)
  #define vcall __vectorcall
#endif

#if defined(__LLVM__)
  #define inline __attribute__((always_inline))
  #define aligned(N) __attribute__((aligned(N)))
#endif

#define null nullptr

#define Assert(Expr) { if (!(Expr)) *(volatile unsigned int*)(nullptr) = 0xDEADBEEF; }
#define ArrayCount(A) (sizeof((A)) / sizeof((A[0])))

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t   i8;
typedef int16_t  i16;
typedef int32_t  i32;
typedef int64_t  i64;

typedef unsigned int uint;
typedef unsigned char byte;
typedef size_t usize;

typedef float  f32;
typedef double f64;


#endif
