#ifndef TETRA_VOXEL_H
#define TETRA_VOXEL_H

#define VOXEL_MESH_INDEX_END 0xFF
#define VOXEL_MESH_COUNT     56

// NOTE: This is *the* canonical representation of cubes in the engine;
// ALL other sources should be rejected and condemned as heresy!
//
//       Y                       (0bZYX)   [X,Y,Z]
//       ^                 A = 0 (0b000) = [0,0,0]
//       |                 B = 1 (0b001) = [1,0,0]
//       C------D          C = 2 (0b010) = [0,1,0]
//       |\     | \        D = 3 (0b011) = [1,1,0]
//       | G------H        E = 4 (0b100) = [0,0,1]
//       A-|----B |--->X   F = 5 (0b101) = [1,0,1]
//        \|     \|        G = 6 (0b110) = [0,1,1]
//         E------F        H = 7 (0b111) = [1,1,1]
//          \
//           \
//            Z
// 
// For the polygonisation of noise, we need to take samples at each of the 8
// corners of an 8-voxel sample cube. We can then use these samples to form
// an 8-bit index into a lookup table of shape configurations.
//
// The lookup table for the 8-voxel sample cells used when polygonising 
// the terrain noise function is generated using the following technique:
//
// Each entry is composed of 8 bytes which is a mask of the shape in each of 
// the 8 corners; one byte per corner. 
//
//    H  G  F  E  D  C  B  A
//    00 00 00 00 00 00 00 00    (HEX)
// 
// The index is given by 8 bits representing whether the corners at those bits
// are filled or not.
//
//    H G F E D C B A
//    0 0 0 0 0 0 0 0            (BIN)
//

// NOTE: Masks out the voxel shape bits (high 3 bits)
#define VOXEL_SHAPE_BITS_MASK 0x38U

// NOTE: Masks out the voxel orientation bits (low 3 bits)
#define VOXEL_ORIENTATION_BITS_MASK 0x07U

// NOTE: High 3 bits of an index determine the shape to use.
enum voxel_type : u8
{
	VT_Tetrahedral          = 0x08,  // 0000 1xxx 
	VT_QuarterPyramidal     = 0x10,  // 0001 0xxx
	VT_HalfCubic            = 0x18,  // 0001 1xxx
	VT_AntiQuarterPyramidal = 0x20,  // 0010 0xxx
	VT_AntiTetrahedral      = 0x28,  // 0010 1xxx
	VT_Cubic                = 0x30,  // 0011 0xxx
    VT_SideHalfCubic        = 0x38   // 0011 1xxx
};

// NOTE: Low 3 bits determine orientation to use
// Bits correspond to the (0,0,0) corner
// the shape is drawn from. 
enum voxel_orientation : u8
{
	VO_BackBottomLeft   = 0x00, // xxxx x000
	VO_BackBottomRight  = 0x01, // xxxx x001
	VO_BackTopLeft      = 0x02, // xxxx x010
	VO_BackTopRight     = 0x03, // xxxx x011
	VO_FrontBottomLeft  = 0x04, // xxxx x100
	VO_FrontBottomRight = 0x05, // xxxx x101
	VO_FrontTopLeft     = 0x06, // xxxx x110
	VO_FrontTopRight    = 0x07  // xxxx x111
};

#endif

