#include <glad/glad.h>
#include <glfw/glfw3.h>
#include <stdio.h>
#include <stdlib.h>

#include "tetra.h"
#include "tetra_math.h"
#include "tetra_data.h"
#include "tetra_world.h"

#define WIN_WIDTH  1280
#define WIN_HEIGHT 720
#define WIN_TITLE "Tetra"
#define WIN_ASPECT 1.77777777f

#define CAM_VELOCITY 0.14f

#define CHUNK_DIM_X 32
#define CHUNK_DIM_Y 32
#define CHUNK_DIM_Z 32

#define WORLD_CHUNKS_X 8
#define WORLD_CHUNKS_Y 8
#define WORLD_CHUNKS_Z 8
#define WORLD_CHUNKS_COUNT ((WORLD_CHUNKS_X)*(WORLD_CHUNKS_Y)*(WORLD_CHUNKS_Z))


static_assert((CHUNK_DIM_X % 2 == 0), "Chunk dimensions must be even");
static_assert((CHUNK_DIM_Y % 2 == 0), "Chunk dimensions must be even");
static_assert((CHUNK_DIM_Z % 2 == 0), "Chunk dimensions must be even");

extern void HandleOpenGLError(GLenum Src, GLenum Type, GLenum ID, GLenum Severity, GLsizei Length, const GLchar* Message, const void* UserData);

struct chunk
{
    uv3 WorldCoords;
    u8  Volume[CHUNK_DIM_X][CHUNK_DIM_Y][CHUNK_DIM_Z];
};

struct chunk_mesh
{
    uint VertexArray;
    uint VertexBuffer;
    uint VertexCount;
};

typedef u32 packed_v3;

struct world_render_data
{
    uint VBO;
    uint VAO;
    uint ChunkEBO[WORLD_CHUNKS_COUNT];
    uint ElementCount[WORLD_CHUNKS_COUNT];
};

static const char* const MainVertexCode = R"GLSL(#version 330 core
                                            layout (location = 0) in vec4 Position;

                                            uniform mat4 V;
                                            uniform mat4 P;
                                            uniform mat4 ChunkP;
                                            
                                            out vs_out {
                                                vec4 TransformedPos;
                                                vec4 ChunkP;
                                            } Out;

                                            void main() {
                                                Out.TransformedPos = P * ChunkP * vec4(Position.xyz, 1.0);
                                                mat4 Transform = P * V * ChunkP;
                                                Out.ChunkP = vec4(ChunkP[3][0], ChunkP[3][1], ChunkP[3][2], 1.0);
                                                gl_Position = Transform * vec4(Position.xyz, 1.0);
                                            })GLSL";

static const char* const MainFragmentCode = R"GLSL(#version 330 core

                                            in vs_out {
                                               vec4 TransformedPos;
                                               vec4 ChunkP;
                                            } In;

                                            out vec4 FragColor;
                                            
                                            void main() {
                                                vec3 Normal = normalize(cross(dFdx(In.TransformedPos.xyz), dFdy(In.TransformedPos.xyz)));
                                                //FragColor = vec4(In.ChunkP.xyz, 1.0);
                                                FragColor = vec4(abs(Normal.xyz), 1.0);
                                                //vec3 LightDir = vec3(1.0, 1.0, 1.0);
                                                //float LightAngle = dot(Normal, LightDir);
                                                //float LightIntensity = max(LightAngle, 0.48);
                                                //FragColor = LightIntensity * vec4(1.0, 0.0, 1.0, 1.0);
                                                //FragColor = vec4(1.0, 0.0, 1.0, 1.0);
                                            })GLSL";

static chunk DEBUGWorldChunks[WORLD_CHUNKS_X][WORLD_CHUNKS_Y][WORLD_CHUNKS_Z];
static bool DEBUGWireframeMode = false;

static world_render_data DEBUGWorldRenderData;


static inline u32
PackChunkIndex(u32 X, u32 Y, u32 Z)
{
    return (Z * CHUNK_DIM_X * CHUNK_DIM_Y) + (Y * CHUNK_DIM_X) + X;
}

static inline packed_v3
PackV3ToU32(u32 X, u32 Y, u32 Z)
{
    return (packed_v3) (0x00U | (Z) | (Y << 10U) | (X << 20U));
}

static inline bool
IsVoxelOccluded(chunk* Chunk, u32 X, u32 Y, u32 Z)
{
    // TODO: Maybe use a 6x byte-mask here for the cube testing?
    if (X > 1 and !(Chunk->Volume[X - 1][Y][Z] & VT_Cubic))                 return false; // West
    if (X < (CHUNK_DIM_X - 1) and !(Chunk->Volume[X + 1][Y][Z] & VT_Cubic)) return false; // East

    if (Y > 1 and !(Chunk->Volume[X][Y - 1][Z] & VT_Cubic))                 return false; // South
    if (Y < (CHUNK_DIM_Y - 1) and !(Chunk->Volume[X][Y + 1][Z] & VT_Cubic)) return false; // North

    if (Z > 1 and !(Chunk->Volume[X][Y][Z - 1] & VT_Cubic))                 return false; // Front
    if (Z < (CHUNK_DIM_Z - 1) and !(Chunk->Volume[X][Y][Z + 1] & VT_Cubic)) return false; // Back

    return true;
}

static inline u64
ComputeCellShape(v3 CellOrigin, u32 ChunkX, u32 ChunkY, u32 ChunkZ)
{
    static const v3 CornerIndices[8] = {
        v3(0.0f, 0.0f, 0.0f),
        v3(2.0f, 0.0f, 0.0f),
        v3(0.0f, 2.0f, 0.0f),
        v3(2.0f, 2.0f, 0.0f),
        v3(0.0f, 0.0f, 2.0f),
        v3(2.0f, 0.0f, 2.0f),
        v3(0.0f, 2.0f, 2.0f),
        v3(2.0f, 2.0f, 2.0f),
    };

    u8 CellShapeIndex = 0x00;
    v3 ChunkOffset = v3((float)ChunkX*CHUNK_DIM_X, (float)ChunkY*CHUNK_DIM_Y, (float)ChunkZ*CHUNK_DIM_Z);

    for (u32 CellCorner = 0; CellCorner < 8; ++CellCorner)
    {
        v3 CornerP = CellOrigin + CornerIndices[CellCorner];
        const f32 Freq = 0.014f;

        v3 WorldP = Freq * (ChunkOffset + v3(CornerP.X, CornerP.Y, CornerP.Z));

        f32 Density = Perlin3(WorldP.X, WorldP.Y, WorldP.Z);

        if (Density > 0.510f)
        {
            CellShapeIndex |= (1 << CellCorner);
        }
    }

    return Data::TerrainCellShapes[CellShapeIndex];
}

static void
GenerateChunk(chunk* Chunk, u32 ChunkX, u32 ChunkY, u32 ChunkZ)
{
    Chunk->WorldCoords = uv3(ChunkX*CHUNK_DIM_X, ChunkY*CHUNK_DIM_Y, ChunkZ*CHUNK_DIM_Z);

    const v3 CornerIndices[8] = {
        v3(0.0f, 0.0f, 0.0f),
        v3(1.0f, 0.0f, 0.0f),
        v3(0.0f, 1.0f, 0.0f),
        v3(1.0f, 1.0f, 0.0f),
        v3(0.0f, 0.0f, 1.0f),
        v3(1.0f, 0.0f, 1.0f),
        v3(0.0f, 1.0f, 1.0f),
        v3(1.0f, 1.0f, 1.0f),
    };

    for (u32 CellX = 0; CellX < CHUNK_DIM_X; CellX += 2)
    {
        for (u32 CellY = 0; CellY < CHUNK_DIM_Y; CellY += 2)
        {
            for (u32 CellZ = 0; CellZ < CHUNK_DIM_Z; CellZ += 2)
            {
                v3 CellOrigin = v3((f32)CellX, (f32)CellY, (f32)CellZ);

                u64 CellShapeData = ComputeCellShape(CellOrigin, ChunkX, ChunkY, ChunkZ);
                for (int Voxel = 0; Voxel < 8; ++Voxel)
                {
                    u64 Density = (CellShapeData >> (8 * Voxel)) & 0xff;
                    
                    v3 VoxelP = CellOrigin + CornerIndices[Voxel];
                    Chunk->Volume[(u32)VoxelP.X][(u32)VoxelP.Y][(u32)VoxelP.Z] = (u8)Density;
                }

            }
        }
    }
}


static uint
CompileShader(const char* VertSrc, const char* FragSrc)
{
    uint VertShader = 0;
    uint FragShader = 0;
    uint Program    = 0;
    int  Success    = false;

    VertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(VertShader, 1, &VertSrc, null);
    glCompileShader(VertShader);
    glGetShaderiv(VertShader, GL_COMPILE_STATUS, &Success);
    
    if (!Success)
    {
        char Log[512] = { 0 };
        glGetShaderInfoLog(VertShader, 512, null, Log);
        fprintf(stderr, "Failed to compile vertex shader\n%s", Log);
        return 0;
    }

    Success = false;
    FragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(FragShader, 1, &FragSrc, null);
    glCompileShader(FragShader);
    glGetShaderiv(FragShader, GL_COMPILE_STATUS, &Success);

    if (!Success)
    {
        char Log[512] = { 0 };
        glGetShaderInfoLog(FragShader, 512, null, Log);
        fprintf(stderr, "Failed to compile fragment shader\n%s", Log);
        return 0;
    }

    Success = false;
    Program = glCreateProgram();
    glAttachShader(Program, VertShader);
    glAttachShader(Program, FragShader);
    glLinkProgram(Program);
    glGetProgramiv(Program, GL_LINK_STATUS, &Success);

    if (!Success)
    {
        fprintf(stderr, "Failed to link shader program\n");
        return 0;
    }

    glDeleteShader(VertShader);
    glDeleteShader(FragShader);

    return Program;
}

extern void
HandleOpenGLError(GLenum Src, GLenum Type, GLenum ID, GLenum Severity, GLsizei Length, const GLchar* Message, const void* UserData)
{
    fprintf(stderr, "[OpenGL Error] %s Type: 0x%x, Sev: 0x%x\n", Message, Type, Severity); 
}

static void
BuildWorldRenderData(chunk* Chunks, uint ChunkCount, world_render_data* DataOut)
{
    // Generate vertex buffer to hold all of the point vertices.
    // There is only one of these per world_render_data instance.
    glGenBuffers(1, &DataOut->VBO);

    // Generate an index buffer for each chunk
    glGenBuffers(ChunkCount, DataOut->ChunkEBO);

    // Generate one vertex array object for the render data.
    glGenVertexArrays(1, &DataOut->VAO);
    glBindVertexArray(DataOut->VAO);

    // Generate point cube data

    // Exact size of the vertex data buffer (bytes)
    // (ChunkDimX*ChunkDimY*ChunkDimZ) Voxels
    // 8 packed_v3s per voxel (one for each of the corner positions)
    const usize VertexDataSize = (usize)(CHUNK_DIM_X*CHUNK_DIM_Y*CHUNK_DIM_Z*sizeof(packed_v3)*8);

    // Bind the data buffer
    glBindBuffer(GL_ARRAY_BUFFER, DataOut->VBO);

    // Allocate space for all of the voxel vertices.
    glBufferData(GL_ARRAY_BUFFER, VertexDataSize, null, GL_STATIC_DRAW);

    packed_v3* VertexData = (packed_v3*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

    const uv3 CornerPositions[8] = {
       uv3(0, 0, 0),
       uv3(1, 0, 0),
       uv3(0, 1, 0),
       uv3(1, 1, 0),
       uv3(0, 0, 1),
       uv3(1, 0, 1),
       uv3(0, 1, 1),
       uv3(1, 1, 1)  
    };

    for (u32 X = 0; X < CHUNK_DIM_X; ++X)
    {
        for (u32 Y = 0; Y < CHUNK_DIM_Y; ++Y)
        {
            for (u32 Z = 0; Z < CHUNK_DIM_Z; ++Z)
            {
                u32 VoxelIndex = PackChunkIndex(X, Y, Z);
                for (u32 VertexIndex = 0; VertexIndex < 8; ++VertexIndex)
                {
                    uv3 CornerPosition = uv3(X, Y, Z) + CornerPositions[VertexIndex];
                    packed_v3 PackedVertex = PackV3ToU32(CornerPosition.X, CornerPosition.Y, CornerPosition.Z);
                    u32 VertexOffset = (VoxelIndex * 8) + VertexIndex;
                    Assert(VertexOffset < VertexDataSize);
                    VertexData[VertexOffset] = PackedVertex;
                }
            }
        }
    }

    glUnmapBuffer(GL_ARRAY_BUFFER);
    glVertexAttribPointer(0, 4, GL_INT_2_10_10_10_REV, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(0);
    
    // Generate an index buffer for each chunk
    const usize MaxIndexDataSize = (CHUNK_DIM_X * CHUNK_DIM_Y * CHUNK_DIM_Z * 36 * sizeof(u32));
    for (u32 ChunkIndex = 0; ChunkIndex < ChunkCount; ++ChunkIndex)
    {
        chunk* CurrentChunk = &Chunks[ChunkIndex];
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, DataOut->ChunkEBO[ChunkIndex]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, MaxIndexDataSize, nullptr, GL_STATIC_DRAW);
        u32* IndexData = (u32*) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

        u32 WrittenIndexCount = 0;
        for (u32 X = 0; X < CHUNK_DIM_X; ++X)
        {
            for (u32 Y = 0; Y < CHUNK_DIM_Y; ++Y)
            {
                for (u32 Z = 0; Z < CHUNK_DIM_Z; ++Z)
                {
                    // Linear offset into the 3D array of the first voxel in this cell of positions.
                    u32 BaseVoxelAbsIndex = PackChunkIndex(X, Y, Z);
                    u8  VoxelType = CurrentChunk->Volume[X][Y][Z];
                    u32 Index;

                    if (not IsVoxelOccluded(CurrentChunk, X, Y, Z))
                    {
                        const u8* VoxelMeshIndices = Data::VoxelMeshes[VoxelType];
                        for (Index = 0; Index < 36 and VoxelMeshIndices[Index] != 0xff; ++Index)
                        {
                            usize IndexWriteOffset = (WrittenIndexCount) + Index;
                            const u32 IndexToWrite = u32((BaseVoxelAbsIndex * 8) + VoxelMeshIndices[Index]);

                            IndexData[IndexWriteOffset] = IndexToWrite;
                        }

                        WrittenIndexCount += Index;
                    }
                }
            }
        }

        glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

        if (WrittenIndexCount == 0) fprintf(stderr, "[WARNING] Zero-index chunk #%i\n", ChunkIndex);
        DataOut->ElementCount[ChunkIndex] = WrittenIndexCount;
        fprintf(stderr, "Index Count: %d\n", WrittenIndexCount);
    }

    glBindVertexArray(0);
}


extern int
main(int ArgCount, const char** const Args)
{
    if (!glfwInit())
    {
        fprintf(stderr, "Failed to initialise GLFW\n");
        return EXIT_FAILURE;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 4); // NOTE: 4x MSAA
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    GLFWwindow* Window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, WIN_TITLE, null, null);
    glfwMakeContextCurrent(Window);
    glfwSwapInterval(1);

    if (!gladLoadGL())
    {
        fprintf(stderr, "Failed to initialise GLAD\n");
        glfwTerminate();
        return EXIT_FAILURE;
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH_CLAMP);

    int FramebufferWidth;
    int FramebufferHeight;
    glfwGetFramebufferSize(Window, &FramebufferWidth, &FramebufferHeight);

    glViewport(0, 0, FramebufferWidth, FramebufferHeight);
    glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    uint Shader = CompileShader(MainVertexCode, MainFragmentCode);

    if (!Shader)
    {
        glfwTerminate();
        return EXIT_FAILURE;
    }

    glUseProgram(Shader);

    int ViewMatrixUniformLocation = glGetUniformLocation(Shader, "V");
    int ProjMatrixUniformLocation = glGetUniformLocation(Shader, "P");
    int ChunkOriginUniformLocation = glGetUniformLocation(Shader, "ChunkP");
    const v3 WorldYAxis = v3(0.0f, 1.0f, 0.0f);

    m4x4 Projection = PerspectiveProjection(Cot45Deg, WIN_ASPECT, 0.01f,  1009.0f);
    glUniformMatrix4fv(ProjMatrixUniformLocation, 1, GL_TRUE, *Projection.M);

    v3 CameraP = v3(0.0f, 0.0f, 5.0f);

    v3 CamForward = v3(0.0f, 0.0f, -1.0f);
    v3 CamRight = v3(1.0f, 0.0f, 0.0f);
    v3 CamUp = v3(0.0f, 1.0f, 0.0f);

    m4x4 CameraTransform = TranslationMatrix(-CameraP);

    f64 LastMouseX, LastMouseY;
    glfwGetCursorPos(Window, &LastMouseX, &LastMouseY);

    // NOTE: Camera yaw and pitch
    f32 Yaw, Pitch;

    // NOTE: Generate world chunks
    f64 StartTime = glfwGetTime();
    for (u32 ChunkX = 0; ChunkX < WORLD_CHUNKS_X; ++ChunkX)
    {
        for (u32 ChunkY = 0; ChunkY < WORLD_CHUNKS_Y; ++ChunkY)
        {
            for (u32 ChunkZ = 0; ChunkZ < WORLD_CHUNKS_Z; ++ChunkZ)
            {
                // XXX: SHOULD INDEX AS ZYX BECAUSE **X** IS THE MOST FREQUENTLY CHANGING
                GenerateChunk(&DEBUGWorldChunks[ChunkZ][ChunkY][ChunkX], ChunkX, ChunkY, ChunkZ);
            }
        }
    }

    f64 Delta = glfwGetTime() - StartTime;
    fprintf(stderr, "Time generating chunks: %g\n", Delta);

    StartTime = glfwGetTime();
    BuildWorldRenderData((chunk*)DEBUGWorldChunks, WORLD_CHUNKS_COUNT, &DEBUGWorldRenderData);
    Delta = glfwGetTime() - StartTime;
    fprintf(stderr, "Time building render data : %g\n", Delta);

    while (!glfwWindowShouldClose(Window))
    {
        glClearColor(0.16f, 0.16f, 0.16f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if (glfwGetKey(Window, GLFW_KEY_Q))
        {
            glfwSetWindowShouldClose(Window, GLFW_TRUE);
        }

        // NOTE: Camera movement controls
        //       Forward: W          Back: S
        //       Right:   D          Left: A
        //       Up:      Space      Down: Left-Shift
        if (glfwGetKey(Window, GLFW_KEY_A)) CameraP -= (CAM_VELOCITY * CamRight);
        if (glfwGetKey(Window, GLFW_KEY_D)) CameraP += (CAM_VELOCITY * CamRight);
        if (glfwGetKey(Window, GLFW_KEY_W)) CameraP += (CAM_VELOCITY * CamForward);
        if (glfwGetKey(Window, GLFW_KEY_S)) CameraP -= (CAM_VELOCITY * CamForward);
        if (glfwGetKey(Window, GLFW_KEY_LEFT_SHIFT)) CameraP -= (CAM_VELOCITY * CamUp);
        if (glfwGetKey(Window, GLFW_KEY_SPACE)) CameraP += (CAM_VELOCITY * CamUp);

        // Debug Keys
        if (glfwGetKey(Window, GLFW_KEY_LEFT_CONTROL))
        {
            if (glfwGetKey(Window, GLFW_KEY_1)) DEBUGWireframeMode = !DEBUGWireframeMode;
        }
        
        { // NOTE: Mouse look
            CamRight = Normalize(Cross(CamForward, WorldYAxis));
            CamUp = Normalize(Cross(CamRight, CamForward));

            f64 MouseX, MouseY;
            glfwGetCursorPos(Window, &MouseX, &MouseY);

            const f32 DX = (f32)(MouseX - LastMouseX);
            const f32 DY = (f32)(MouseY - LastMouseY);

            LastMouseX = MouseX;
            LastMouseY = MouseY;

            Yaw = Rads(DX) * -0.05f;
            Pitch = Rads(DY) * -0.05f;

            quat YawRotation = RotationQuaternion(Yaw, WorldYAxis);
            quat PitchRotation = RotationQuaternion(Pitch, CamRight);

            CamForward = Normalize(Rotate((YawRotation * PitchRotation), CamForward));
        }

        m4x4 T = TranslationMatrix(-CameraP);
        m4x4 R = {{
            { CamRight.X, CamRight.Y, CamRight.Z, 0.0f },
            { CamUp.X,    CamUp.Y,    CamUp.Z,    0.0f },
            { -CamForward.X, -CamForward.Y, -CamForward.Z, 0.0f },
            { 0.0f, 0.0f, 0.0f, 1.0f }
        }};

        CameraTransform =  R * T;
        glUniformMatrix4fv(ViewMatrixUniformLocation, 1, GL_TRUE, *CameraTransform.M);

        if (DEBUGWireframeMode)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }
        else
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        glBindVertexArray(DEBUGWorldRenderData.VAO);
        glBindBuffer(GL_ARRAY_BUFFER, DEBUGWorldRenderData.VBO);

        u32 ChunkIndex = 0;
        for (u32 ChunkX = 0; ChunkX < WORLD_CHUNKS_X; ++ChunkX)
        {
            for (u32 ChunkY = 0; ChunkY < WORLD_CHUNKS_Y; ++ChunkY)
            {
                for (u32 ChunkZ = 0; ChunkZ < WORLD_CHUNKS_Z; ++ChunkZ)
                {
                    uv3 ChunkP = DEBUGWorldChunks[ChunkX][ChunkY][ChunkZ].WorldCoords;
                    m4x4 ChunkTransform = TranslationMatrix(ChunkX*CHUNK_DIM_X, ChunkY*CHUNK_DIM_Y, ChunkZ*CHUNK_DIM_Z);
                    glUniformMatrix4fv(ChunkOriginUniformLocation, 1, GL_TRUE, *ChunkTransform.M); 
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, DEBUGWorldRenderData.ChunkEBO[ChunkIndex]);
                    glDrawElements(GL_TRIANGLES, DEBUGWorldRenderData.ElementCount[ChunkIndex], GL_UNSIGNED_INT, nullptr);
                        
                    ++ChunkIndex;
                }
            }
        }

        glBindVertexArray(0);

        glfwPollEvents();
        glfwSwapBuffers(Window);
    }
    
    glDeleteBuffers(WORLD_CHUNKS_COUNT, DEBUGWorldRenderData.ChunkEBO);
    glDeleteBuffers(1, &DEBUGWorldRenderData.VBO);
    glDeleteVertexArrays(1, &DEBUGWorldRenderData.VAO);

    glUseProgram(0);
    glDeleteShader(Shader);
    glfwTerminate();

    return EXIT_SUCCESS;
}
